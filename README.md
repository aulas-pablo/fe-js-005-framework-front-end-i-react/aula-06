# aula-06


O objetivo é entender o funcionamento do `Context` como gerenciamento de estado global.



- Deverá implementar um componente `ProductItem` que irá renderizar `avatar = image[0], title = nome do produto, price = preço do produto`. Implementar também um botao que remove um produto do carrinho.

- Deverá implementar um componente `Cart` que irá renderizar um lista de `ProductItem` com os produtos adicionados ao `carrinho de compras`. 

- Deverá calcular a soma dos produtos adicionados ao `carrinho de compras` e mostrar o valor total da compra.

- Deverá implementar um botao `limpar carrinho` que ao clicar deve remover todos produtos adicionados ao carrinho.

- Deverá implementar um icone ccom o simbolo de `carrinho` no heander com um `badge` com a quantidade de itens no carrinho e ao clicar deverá ir para tela do carrinho mostrando os produtos adicionados.

#### obs: utilizar localStorage para salvar dados do carrinho